import sys

from voscrape import SignASLScraper

def main():
    h = SignASLScraper()
    print "----------------------------"
    print "Short way:: using the find_sign_video function"
    vs = h.find_sign_video("about")
    print vs
    print "----------------------------"
    print "Downloading video"
    print "File location: %s" % h.download_video(vs[0]["url"], vtype=vs[0]["url"])
    print "----------------------------"


    print "----------------------------"
    print "Short way:: using the find_sign_video function"
    vs = h.find_sign_video("like")
    print vs
    print "----------------------------"
    print "Downloading video"
    print "File location: %s" % h.download_video(vs[0]["url"], override=True, save_as="like.mp4")
    print "----------------------------"


if __name__ == '__main__':
    main()
