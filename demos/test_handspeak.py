import sys

from voscrape import HandSpeakScraper

def main():
    h = HandSpeakScraper()
    print "----------------------------"
    print "Short way:: using the find_sign_video function"
    w = h.find_sign_video("about")
    print w
    print "----------------------------"
    print "Long way:: using the find function"
    w = h.find("chicken")
    print w
    vs = h.get_video_url_by_id(w["signID"])
    print vs
    print "File location: %s" % h.download_video(vs[0]["url"])
    print "----------------------------"


if __name__ == '__main__':
    main()
