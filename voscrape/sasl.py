
import urllib
import requests
import argparse
import json
from bs4 import BeautifulSoup

from tsearch import binary_search
from base import BaseScraper


class SignASLScraper(BaseScraper):
    session = None
    word_search_url = "http://www.signasl.org/sign"
    base_url = "http://www.signasl.org"


    def find_sign_video(self, word):
        req = requests.get("%s/%s" % (self.word_search_url, word))
        data = req.content
        data = data.replace("\r\n", "").replace("\t", "")
        soup = BeautifulSoup(data, "html.parser")
        dh_videos = soup.select("div[itemprop=\"video\"]")
        videos = []
        for dh_video in dh_videos:
            h_video = dh_video.select("video")
            if h_video:
                h_video = h_video[0]
                video = {
                    "signID": word,
                    "vType": "normal"
                }
                video["url"] = "%s" % h_video.select("source")[0].get("src")
            else:
                h_video = dh_video.select("iframe")
                if h_video:
                    h_video = h_video[0]
                    video = {
                        "signID": word,
                        "vType": "youtube"
                    }
                    yvid = h_video.get("src")
                    if yvid.startswith("//"):
                        yvid = "https:%s" % yvid
                    video["url"] = "%s" % yvid
                else:
                    continue
            t_video = dh_video.select("div i")
            video["title"] = word
            video["description"] = t_video[0].get_text() if t_video else ""
            videos.append(video)
        return videos
