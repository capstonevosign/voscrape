
import urllib
import requests
import argparse
import json
from bs4 import BeautifulSoup

from tsearch import binary_search
from base import BaseScraper


class HandSpeakScraper(BaseScraper):
    status = 200
    session = None
    word_search_url = "http://www.handspeak.com/word/search/index.php"
    wordlist_url = "http://www.handspeak.com/word/search/app/getlist.php"
    video_base_url = "http://www.handspeak.com/word/{}/{}.mp4"
    base_url = "http://www.handspeak.com/"

    def get_wordlist(self):
        if not hasattr(self, "__wordlist"):
            req = requests.get(self.wordlist_url)
            if req.status_code == 200:
                self.__wordlist = json.loads(req.content)
                return self.__wordlist
            return []
        self.__wordlist


    def find_sign_video(self, word):
        sign_data = self.find(word)
        if not sign_data:
            return {}
        return self.get_video_url_by_id(sign_data["signID"])


    def find(self, word):
        def get_sign_data(item):
            return word.lower() == item["signName"]
        wordlist = self.get_wordlist()
        sign = binary_search(wordlist, get_sign_data)
        if not sign:
            return {}
        return sign


    def get_video_url_by_id(self, sign_id):
        req = requests.get(self.word_search_url, params={"id": sign_id})
        if req.status_code == 200:
            data = req.content
            data = data.replace("\r\n", "").replace("\t", "")
            soup = BeautifulSoup(data, "html.parser")
            h_videos = soup.select(".grid-dictbox video")
            d_videos = soup.select("div.dictext p")
            videos = []
            for i,h_video in enumerate(h_videos):
                video = {
                    "signID": sign_id
                }
                video["url"] = "%s%s" % (self.base_url, h_video.get("src"))
                video["title"] = h_video.get("title")
                video["description"] = "" if not d_videos and i < len(d_videos) else d_videos[i].get_text()
                videos.append(video)
            return videos
        return []
