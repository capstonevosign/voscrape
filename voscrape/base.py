import os
import re
import requests
from bs4 import BeautifulSoup
from pytube import YouTube


class BaseScraper:

    def download_youtube_video(self, video_url, save_as=None, directory=".", override=False):
	# import pdb; pdb.set_trace()
        req = requests.get(video_url)
        data = req.content
        data = data.replace("\r\n", "").replace("\t", "")
        soup = BeautifulSoup(data, "html.parser")
        ytube = [a.get("href") for a  in soup.select("link[href]") if "youtube.com" in a.get("href")]
        if not ytube:
            return ""
        yt = YouTube(ytube[0])
	if save_as:
            yt.set_filename(os.path.basename(re.sub("\.(M|m)(P|p)4", "", save_as)))
	else:
	    save_as = "{}.mp4".format(yt._filename) if "mp4" not in yt._filename else yt._filename
        vt = [a for a in yt.get_videos() if a.extension.lower() == "mp4"]
        if not vt:
            return ""
        video = yt.get('mp4', vt[0].resolution)
        video.download(directory, force_overwrite=True)
        return save_as


    def download_video(self, video_url, save_as=None, directory=".", override=False, vtype=""):
        name = save_as if save_as else os.path.basename(video_url)
        file_path = "%s/%s" % (directory, name)
        if not os.path.isfile(file_path) or override:
            if re.search("youtube.*\.com", video_url.lower()) or (vtype and vtype.lower() == "youtube"):
		print "Downloading youtube videos."
                return self.download_youtube_video(video_url, save_as=save_as, directory=directory, override=override)
            req = requests.get(video_url)
            with open(file_path, "wb") as fp:
                fp.write(req.content)
        return file_path
