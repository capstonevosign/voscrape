def binary_search(lst, comparator):
    """Perform a binary search of the given list based on the given comparator

    Arguments:
        lst (list):
            List of items to search through.
        comparator (function):
            Reference pointer for the function or method. This function should
            only accept one parameter which is a signle list item. In addition,
            this function should return a boolean value or none.

    Example:
        Create a list of integers to pass in as the search list::

            a = [3,5,6,2,1]

        Create a function named `num_check` to check if consist of
        the number 2. Note the `num_check` function will be the comparator
        function of the `binary_search` function::

            def num_check(num):
                return num == 2

        Perform a binary search on the list `a`::

            result = binary_search(a, num_check)

        The value 2 will be stored in the  `result` variable because 2 is in
        the list `a`. However, if we had check `num == 10`, then `None` would
        be return from the `binary_search` function.

    Returns:
        The value that matches the comparator cretria.
    """
    # Get the lower limit of the list.
    l_limit = 0
    # Get the upper limit of the list.
    u_limit = len(lst)

    # If the upper limit is zero or equal to the lower limit
    # return None.
    if u_limit == 0 or l_limit == u_limit:
        return None

    # Check if the first element of the list is valid based
    # on the given comparator.
    value = lst[0]
    if comparator(value):
        return value

    # Check if the middle element of the list is valid based
    # on the given comparator.
    m_limit = (l_limit + u_limit) / 2
    value = lst[m_limit]
    if comparator(value):
        return value

    # Recursively call the binary search function based on
    # the lower half of the list.
    value = binary_search(lst[:m_limit], comparator)
    if value:
        # If a value is found for the lower half, return it.
        return value
    else:
        # Check the upper half of the list for the value.
        return binary_search(lst[m_limit + 1:], comparator)
