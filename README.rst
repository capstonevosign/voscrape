Voscrape - Sign Language Screen Scraper
---------------------------------------

Voscrape is used to search for sign language words on various datasets such as HandSpeak and SignASL database.

Setup Instruction
=================

There are two ways to install voscrape into your application.

1.
    By using pip
    ::
        pip install git+https://b4oshany@bitbucket.org/capstonevosign/voscrape.git

2.
    By manually download it via git then run the setup file
    ::
        git clone https://b4oshany@bitbucket.org/capstonevosign/voscrape.git
        cd voscrape
        python setup.py install


Example 1
=========
    The snippet below is used to search and download the sign language video for **chicken**
    using signasl.org Database::
        from voscrape import SignASLScraper

        # Create a SignASLScraper object
        h = SignASLScraper()
        # Get the video url for a given sign
        video_sign_data = h.find_sign_video("about")
        print video_sign_data
        # Download sign
        print "File location: " % h.download_video(video_sign_data[0]["url"])

    Output is
    ::
        # Create a HandSpeakScraper object=
        [{
            'url': u 'http://www.aslpro.com/mobile/main/m/most.mp4',
            'signID': 'about',
            'description': u 'most',
            'title': 'about'
        }, {
            'url': u 'http://www.aslpro.com/mobile/main/a/almost.mp4',
            'signID': 'about',
            'description': u 'almost',
            'title': 'about'
        }, {
            'url': u 'http://www.aslpro.com/mobile/main/n/near.mp4',
            'signID': 'about',
            'description': u 'near',
            'title': 'about'
        }, {
            'url': u 'http://media.signbsl.com/videos/asl/tsdvideo/mp4/1045.mp4',
            'signID': 'about',
            'description': u 'most',
            'title': 'about'
        }, {
            'url': u 'http://media.signbsl.com/videos/asl/tsdvideo/mp4/140.mp4',
            'signID': 'about',
            'description': u 'most',
            'title': 'about'
        }, {
            'url': u 'http://media.signbsl.com/videos/asl/tsdvideo/mp4/1047.mp4',
            'signID': 'about',
            'description': u 'near',
            'title': 'about'
        }, {
            'url': u 'http://media.signbsl.com/videos/asl/tsdvideo/mp4/378.mp4',
            'signID': 'about',
            'description': u 'near',
            'title': 'about'
        }, {
            'url': u 'http://www.aslpro.com/mobile/main/a/about.mp4',
            'signID': 'about',
            'description': u 'about',
            'title': 'about'
        }, {
            'url': u 'http://www.aslpro.com/mobile/main/s/some.mp4',
            'signID': 'about',
            'description': u 'some',
            'title': 'about'
        }]
        File location: ./most.mp4



Example 2
=========
    The snippet below is used to search and download the sign language video for **chicken**
    using HandSpeak.com Database::
        from voscrape import HandSpeakScraper

        # Create a HandSpeakScraper object
        h = HandSpeakScraper()
        # Get the video url for a given sign
        video_sign_data = h.find_sign_video("chicken")
        print video_sign_data
        # Download sign
        print "File location: " % h.download_video(video_sign_data[0]["url"])

    Output is
    ::
        # Create a HandSpeakScraper object=
        {u'menuID': u'c', u'signID': u'382', u'signName': u'chicken'}
        [{
            'url': u 'http://www.handspeak.com//word/c/chicken.mp4',
            'signID': u '382',
            'description': u '\nchicken; domestic fowl\n',
            'title': u 'ASL sign for chicken'
        }]
        File location: ./chicken.mp4
